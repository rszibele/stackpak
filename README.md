# stackpak

## As of 01.11.2019 deprecated / unmaintained

Use cabal-flatpak from Henning Thielemann, it's inspired by stackpak and uses the new Cabal V2 build system.

https://hackage.haskell.org/package/cabal-flatpak
